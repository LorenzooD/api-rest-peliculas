package com.peliculas.model

import kotlinx.serialization.Serializable

@Serializable
class Pelicula(
    var id : String,
    var titulo : String,
    var genero : String,
    var anyo : String,
    var director : String,
    val comentarios : MutableList<Comentario>
)

val carteleraPelicula = mutableListOf<Pelicula>()