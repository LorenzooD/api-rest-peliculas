package com.peliculas.model

import kotlinx.serialization.Serializable
import java.time.LocalDateTime

@Serializable
class Comentario(
    var id : String,
    var idPelicula : String,
    val comentario : String,
    var dataCreacion : String,
){
    init {
        dataCreacion = LocalDateTime.now().toString()
    }
}