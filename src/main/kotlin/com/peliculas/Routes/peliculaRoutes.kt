package com.peliculas.Routes

import com.peliculas.model.Comentario
import com.peliculas.model.Pelicula
import com.peliculas.model.carteleraPelicula
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlin.text.get

fun Route.peliculaRoutes() {
    route("/peliculas") {

        //AÑADIR COMENTARIO
        get {
            if (carteleraPelicula.isNotEmpty()) call.respond(carteleraPelicula)
            else call.respondText("No se han encontrado peliculas")
        }

        //AÑADIR COMENTARIO
        get ("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) {
                return@get call.respondText("Id de pelicula incorrecta")
            }
            for (pelicula in carteleraPelicula) {
                if (pelicula.id == call.parameters["id"]) return@get call.respond(pelicula)
            }
            call.respondText("No se ha encontrado la id ${call.parameters["id"]}", status = HttpStatusCode.NotFound)
        }

        //AÑADIR COMENTARIO
        post {
            val pelicula = call.receive<Pelicula>()
            pelicula.id = (carteleraPelicula.size + 1).toString()
            carteleraPelicula.add(pelicula)
            call.respondText("Se ha añadido la pelicula correctamente", status = HttpStatusCode.Created)
        }

        //AÑADIR COMENTARIO
        post("{id?}") {
            if (call.parameters["id"].isNullOrBlank()){
                return@post call.respondText("Id de la película incorrecta", status = HttpStatusCode.BadRequest)
            }
            val id = call.parameters["id"]
            for (pelicula in carteleraPelicula) {
                if (pelicula.id == id) {
                    val comentari = call.receive<Comentario>()
                    comentari.id = (pelicula.comentarios.size + 1).toString()
                    comentari.idPelicula = id
                    pelicula.comentarios.add(comentari)
                    return@post call.respondText("¡Comentario de la película ${pelicula.titulo} añadido correctamente!",status = HttpStatusCode.Created )
                }
            }
            call.respondText("No se ha encontrado la id $id en la cartelera", status = HttpStatusCode.NotFound)
        }

        //AÑADIR COMENTARIO
        put("{id?}") {
            if (call.parameters["id"].isNullOrBlank()){
                return@put call.respondText("Id de la película incorrecta.", status = HttpStatusCode.BadRequest)
            }
            val id = call.parameters["id"]
            val peliculaPerModificar = call.receive<Pelicula>()
            for (pelicula in carteleraPelicula) {
                if (pelicula.id == id) {
                    pelicula.titulo = peliculaPerModificar.titulo
                    pelicula.anyo = peliculaPerModificar.anyo
                    pelicula.genero = peliculaPerModificar.genero
                    pelicula.director = peliculaPerModificar.director
                    return@put call.respondText(
                        "Se ha actualizado la película con el título:  ${pelicula.titulo}",
                        status = HttpStatusCode.Accepted
                    )
                }
            }
            call.respondText("No se ha encontrado la id $id en la cartelera", status = HttpStatusCode.NotFound)
        }

        //AÑADIR COMENTARIO
        delete("{id?}") {
            if (call.parameters["id"].isNullOrBlank()){
                return@delete call.respondText("Id de la pelicula incorrecta", status = HttpStatusCode.BadRequest)
            }
            val id = call.parameters["id"]
            for (pelicula in carteleraPelicula) {
                if (pelicula.id == id) {
                    carteleraPelicula.remove(pelicula)
                    return@delete call.respondText("Se ha eliminado ${pelicula.titulo} de la cartelera", status = HttpStatusCode.Accepted)
                }
            }
            call.respondText("Id $id no encontrada en la cartelera." , status = HttpStatusCode.NotFound)
        }

        //AÑADIR COMENTARIO
        get("{id?}/comentaris"){
            if (call.parameters["id"].isNullOrBlank()){
                return@get call.respondText("Id de la película incorrecto", status = HttpStatusCode.BadRequest)
            }
            for (pelicula in carteleraPelicula){
                if(pelicula.id == call.parameters["id"]){
                    return@get call.respond(pelicula.comentarios)
                }
            }
            call.respondText("Comentarios para la id  ${call.parameters["id"]} no encontrados. ", status = HttpStatusCode.NotFound)
        }
    }
}