package com.peliculas.plugins

import com.peliculas.Routes.peliculaRoutes
import io.ktor.server.routing.*
import io.ktor.server.response.*
import io.ktor.server.application.*

fun Application.configureRouting() {
    routing {
        peliculaRoutes()
    }
}
